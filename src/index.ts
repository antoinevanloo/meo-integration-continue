import {createConnection} from "typeorm";
require('dotenv').config();
import server from "./server";

async function bootstrap () {
    try {
        await createConnection();
        server.listen(process.env.PORT, () => {
            console.log('app listening on port ' + process.env.PORT)
        });
    } catch (error) {
        console.error(error);
    }
}

bootstrap();
