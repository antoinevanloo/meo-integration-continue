import request from "supertest";
import server from "../../src/server";
import {createConnection, getConnection} from "typeorm";

let connexion;

beforeAll(async done => {
    connexion = await createConnection();
    done();
});

describe("Test server routes", () => {
    it("GET / respond Home", done => {
        request(server)
            .get("/")
            .expect("Content-Type", /text\/html/)
            .expect("Home")
            .expect(200, done);
    });

    it("GET /users respond with json", done => {
        request(server)
            .get("/users")
            .expect("Content-Type", /json/)
            .expect(200, done);
    });
});

afterAll(async done => {
    await connexion.close();
    done();
});
